﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using DigitalPermits.Framework.Tracing;
using NDesk.Options;

namespace DigitalPermits.BulkEmailer {
	class Program
	{
        private static ITraceWriter _traceWriter = new TraceWriter("Program");
		private static OptionSet _options;

        /// <summary>
        /// Testing Instructions
        /// 
        /// Insure latest EntityModel is included (has public IEnumerable<UserAccount> ListAdminUsers())
        /// Put a break point in constructor close bracket and verify all list counts match queries in DB
        /// First try these admin runs 
        /// param 50 (Selected Engineers) param 51 (selected AMs) 
        /// then    admin groups in order: 0 1 2 3 4 5
        ///         motorist clients 6 9 11 12
        ///         (see Notify Security Policy for sll property descriptions)
        /// 
        /// Verify For Each run: 
        ///     check c:\temp for each params run "C:\\temp\\SecurityPolicyNotificationsForParam_<param>_timestamp.csv"
        ///     query the history table with type = email 
        ///     if running locally check c:\temp for email contents 
        ///     verify links to urls match according to param 
        /// 
        /// First runs in prod use these params to target selected accounts and review emails: param 50 (Selected Engineers) param 51 (selected AMs) 
        /// 
        /// </summary>
        /// <param name="args"></param>
		static void Main(string[] args)
		{
			var settings = new Settings();

			PrintPreamble();
			InitCommandLineOptions(settings);

			var isOptionsValid = ParseCommandLineOptions(args, settings);
			if (!isOptionsValid || args.Length == 0 || settings.ShowHelp)
			{
				PrintUsage();
				Environment.Exit(0);
			}

            if (PrintExtraInformation(settings))
			{
				Environment.Exit(0);
			}

			var usersToNotify = new NotifySecurityPolicy(settings);

			var totalEmailSent = 0;
			if (settings.TestEmail) {
				totalEmailSent = usersToNotify.SendTestEmail();

				logEmailsSent(totalEmailSent);
				Environment.Exit(0);
			}

			// send out bulk email
			totalEmailSent = usersToNotify.NotifyInteractiveUsers();

			logEmailsSent(totalEmailSent);

			Environment.Exit(0);
		}

		private static void logEmailsSent(int totalEmailSent) {
            _traceWriter.WriteTrace(TraceEventType.Information, $"Total Email Sent = {totalEmailSent}");
			Console.WriteLine("Total Email Sent = {0}", totalEmailSent);
		}

		private static void InitCommandLineOptions(Settings settings)
		{
			_options = new OptionSet() {
				{ "l|list", "lists all of the available client IDs", v => { settings.ListClientIds = v != null; }},
				{ "c|client=", "the {ClientId} or admin group params for the bulk emails", (int v) => { settings.ClientId = v; }},
				{ "h|help", "prints this options list", v => { settings.ShowHelp = v != null; }},
				{ "p|print", "prints a list of all notified users for a client", v => { settings.PrintNotificationList = v != null; }},
				{ "n|noemail", "do not email users", v => { settings.DoNotEmail = v != null; }},
				{ "t|testemail", "test that the email can be created and sent", v => { settings.TestEmail = v != null; }}
			};
		}

		private static void PrintUsage() {
			Console.WriteLine("Usage: BulkEmailer.exe [options]");
			_options.WriteOptionDescriptions(Console.Out);
		}

		private static void PrintPreamble()
		{
			Console.WriteLine("BulkEmailer v0.1 - The Digital Parking Permit Security Bulk Email Tool");
			Console.WriteLine("Copyright (c) 2017");
		}

		private static bool ParseCommandLineOptions(IEnumerable<string> args, Settings settings) {
			try {
				var firstarg = _options.Parse(args);
                int clientId;
                if (int.TryParse(firstarg[0], out clientId))
                {
                    settings.ClientId = clientId;
                }
                else
                {
                    Console.WriteLine("First argumant must be an integer");
                }
            }
			catch (OptionException e) {
				Console.Write("Error: ");
				Console.WriteLine(e.Message);
				Console.WriteLine("Try 'BulkEmailer --help' for more information.");
				return false;
			}

			return true;
		}

		private static bool PrintExtraInformation(Settings settings) {
			if (settings.ListClientIds) {

                //TODO: figure out how its used and if its needed
				//var clients = expiredPermits.GetClients();

				//foreach (var client in clients) {
				//	Console.WriteLine("\t{0} - {1}", client.Id.ToString(CultureInfo.InvariantCulture).PadRight(3), client.Name);
				//}
				return true;
			}

			return false;
		}
	}
}
