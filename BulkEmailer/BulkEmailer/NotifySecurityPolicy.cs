﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using DigitalPermits.EntityModel.Entities;
using DigitalPermits.EntityModel.CustomTypes;
using DigitalPermits.EntityModel.Repositories;
using DigitalPermits.Framework.Tracing;
using DigitalPermits.BulkEmailer.Email;
using System.Text;
using System.IO;

namespace DigitalPermits.BulkEmailer
{
    public class NotifySecurityPolicy
    {
        private ITraceWriter _traceWriter;

        private readonly Settings _settings;
        private readonly IAdminRepository _adminRepository;
        private readonly INotifier _notifier;
        private const int ADMINPAYLOCK = 0;
        private const int ADMINDIGITALPERMITSAGENT = 1;
        private const int ADMINPGC = 2;
        private const int ADMINMCO = 3;
        private const int ADMINNB = 4;
        private const int ADMINOTHER = 5;
        private const int ADMINTESTENGINEERS = 50;
        private const int ADMINTESTACCOUNTMANAGERS = 51;
        private const int ADMINTESTDIGITALPERMITSAGENTS = 52;
        private const int MOTORISTSTESTFROMPGC = 53;
        private const int MOTORISTSTESTFROMJAVE = 54;
        private const int MOTORISTSTESTFROMMCO = 55;
        private const int MOTORISTSTESTFROMNB = 56;
        private const int CLIENTPGC = 6;
        private const int CLIENTMCO = 9;
        private const int CLIENTNB = 11;
        private const int CLIENTJAVE = 12;
        private List<Motorist> _motoristsFromPGC;
        private List<Motorist> _motoristsFromMCO;
        private List<Motorist> _motoristsFromNB;
        private List<Motorist> _motoristsFromJAVE;
        private List<UserAccount> _adminsAll;
        private List<UserAccount> _adminsFromPaylock;
        private List<UserAccount> _adminsFromDigitalPermitsAgent;
        private List<UserAccount> _adminsFromPGC;
        private List<UserAccount> _adminsFromMCO;
        private List<UserAccount> _adminsFromNB;
        private List<UserAccount> _adminsFromOther;
        private List<UserAccount> _adminTestEngineers;
        private List<UserAccount> _adminTestAccountManagers;
        private List<UserAccount> _adminTestDigitalPermitsAgents;
        private List<Motorist> _motoristsTestFromPGC;
        private List<Motorist> _motoristsTestFromMCO;
        private List<Motorist> _motoristsTestFromNB;
        private List<Motorist> _motoristsTestFromJAVE;
        private Client _motoristClient;
        private const int MINCLIENTID = 6;
        private const int MAXCLIENTID = 12;

        public NotifySecurityPolicy(Settings settings, IAdminRepository adminRepository, INotifier notifier)
        {
            _settings = settings;
            _adminRepository = adminRepository;
            _notifier = notifier;
            _traceWriter = new TraceWriter("SecurityPolicyUpdate");
        }

        public NotifySecurityPolicy(Settings settings)
        {
            _settings = settings;
            _adminRepository = new AdminRepository();
            _notifier = new Notifier();
            _traceWriter = new TraceWriter("SecurityPolicyUpdate");
            _motoristClient = GetMotoristClient();
            _motoristsFromPGC = _adminRepository.ListClientMotorists(CLIENTPGC).ToList();
            _motoristsFromMCO = _adminRepository.ListClientMotorists(CLIENTMCO).ToList();
            _motoristsFromMCO = _motoristsFromMCO.Except(_motoristsFromPGC).ToList();
            _motoristsFromNB = _adminRepository.ListClientMotorists(CLIENTNB).ToList();
            _motoristsFromNB = _motoristsFromNB.Except(_motoristsFromPGC).ToList();
            _motoristsFromNB = _motoristsFromNB.Except(_motoristsFromMCO).ToList();
            _motoristsFromJAVE = _adminRepository.ListClientMotorists(CLIENTJAVE).ToList();
            _motoristsFromJAVE = _motoristsFromJAVE.Except(_motoristsFromPGC).ToList();
            _motoristsFromJAVE = _motoristsFromJAVE.Except(_motoristsFromMCO).ToList();
            _motoristsFromJAVE = _motoristsFromJAVE.Except(_motoristsFromNB).ToList();
            _adminsAll = _adminRepository.ListAdminUsers().ToList();
            _adminsFromPaylock = _adminsAll.Where(u => u.Email.Contains("@paylock.com")).ToList();
            _adminsFromDigitalPermitsAgent = _adminsAll.Where(u => u.Email.Contains("@digitalpermitsagent.com")).ToList();
            _adminsFromPGC = _adminsAll.Where(u => u.Email.Contains("@co.pg.md.us")).ToList();
            _adminsFromMCO = _adminsAll.Where(u => u.Email.Contains("@montgomerycountymd.gov")).ToList();
            _adminsFromNB = _adminsAll.Where(u => u.Email.Contains("@njnbpa.org")).ToList();
            _adminsFromOther = _adminsAll.Where(u => u.Email.Contains("@xerox.com") || u.Email.Contains("@conduent.com") || u.Email.Contains("@serco-na.com")).ToList();
            _adminTestEngineers = _adminsFromPaylock.Where(u => u.Email.Contains("gsanford@") || u.Email.Contains("vrobles@") || u.Email.Contains("jyoo@")).ToList();
            _adminTestAccountManagers = _adminsFromPaylock.Where(u => u.Email.Contains("cdorner@") || u.Email.Contains("scoleman@")).ToList();
            _adminTestDigitalPermitsAgents = _adminsFromDigitalPermitsAgent.Where(u => u.Email.Contains("@xxdigitalpermitsagent.com")).ToList();
            _motoristsTestFromPGC = _motoristsFromPGC.Where(u => u.Email.Contains("gsanford@paylock.com")
                                                                                   || u.Email.Contains("jyoo@paylock.com")
                                                                                   || u.Email.Contains("vrobles@paylock.com")
                                                                                   || u.Email.Contains("scoleman@paylock.com")
                                                                                   || u.Email.Contains("6a933d96d585432b8e195209372f83d9Guest@DigitalPermits.com")
                                                                                   || u.Email.Contains("370f32d52f4a42358cb4ede740e9a1aeGuest@DigitalPermits.com")
                                                                                   || u.Email.Contains("cdorner@paylock.com")).ToList();
            _motoristsTestFromMCO = _motoristsFromMCO.Where(u => u.Email.Contains("gsanford@paylock.com")
                                                                                   || u.Email.Contains("jyoo@paylock.com")
                                                                                   || u.Email.Contains("gsandym@gmail.com")
                                                                                   || u.Email.Contains("scoleman@paylock.com")
                                                                                   || u.Email.Contains("huoc@hotmail.com")).ToList();
            _motoristsTestFromNB = _motoristsFromNB.Where(u => u.Email.Contains("gsanford@paylock.com")
                                                                                   || u.Email.Contains("jyoo@paylock.com")
                                                                                   || u.Email.Contains("vrobles@paylock.com")
                                                                                   || u.Email.Contains("scoleman@paylock.com")
                                                                                   || u.Email.Contains("huoc@hotmail.com")).ToList();
            _motoristsTestFromJAVE = _motoristsFromJAVE.Where(u => u.Email.Contains("gsanford@paylock.com")
                                                                                   || u.Email.Contains("jyoo@paylock.com")
                                                                                   || u.Email.Contains("vrobles@paylock.com")
                                                                                   || u.Email.Contains("scoleman@paylock.com")
                                                                                   || u.Email.Contains("cdorner@paylock.com")).ToList();
        }


        /// <summary>
        /// This is the main entry point 
        /// </summary>
        public int NotifyInteractiveUsers()
        {
            if (((_settings.ClientId >= MINCLIENTID) && (_settings.ClientId <= MAXCLIENTID)) || (_settings.ClientId > 52))
            {
                return NotifyInteractiveMotorists();
            } 
            else
            {
                return NotifyInteractiveAdmins();
            }
        }

        private int NotifyInteractiveMotorists()
        {
            // non interactive users are filtered out in the send so we can still log them
            _traceWriter.WriteTrace(TraceEventType.Information, $"*** Notifying Interactive Users of security policy for client {_settings.ClientId.ToString()}  ***");

            int totalEmailsSent = 0;
            // list of motorists or user accounts
            var activeMotorists = GetActiveMotoristsByClient(_settings.ClientId);
            if (activeMotorists == null)
            {
                _traceWriter.WriteTrace(TraceEventType.Information, $"*** Invalid client Id {_settings.ClientId.ToString()} sent to mail Interactive Users of security policy value = ***");
                return 0;
            }

            var distinctEmailAccounts = activeMotorists.GroupBy(u => u.Email).Select(grp => grp.First()).ToList();

            totalEmailsSent = NotifyUsersAboutSecurityPolicyUpdate(distinctEmailAccounts);

            _traceWriter.WriteTrace(TraceEventType.Information, $"*** Total Emails sent regarding security policy: {totalEmailsSent} ***");

            return totalEmailsSent;
        }

        private int NotifyInteractiveAdmins()
        {
            // non interactive users are filtered out in the send so we can still log them
            _traceWriter.WriteTrace(TraceEventType.Information, $"*** Notifying Interactive Users of security policy for client {_settings.ClientId.ToString()}  ***");


            var activeUsers = GetActiveAdminsByCategory(_settings.ClientId);
            if (activeUsers == null)
            {
                _traceWriter.WriteTrace(TraceEventType.Information, "*** Invalid client Id sent to mail Interactive Users of security policy ***");
                return 0;
            }

            var distinctEmailAccounts = activeUsers.GroupBy(u => u.Email).Select(grp => grp.First()).ToList();

            var totalEmailsSent = NotifyUsersAboutSecurityPolicyUpdate(distinctEmailAccounts);
            _traceWriter.WriteTrace(TraceEventType.Information, $"*** Total Emails sent regarding security policy: {totalEmailsSent} ***");

            return totalEmailsSent;
        }


        private List<Motorist> GetActiveMotoristsByClient(int clientId)
        {
            switch (clientId)
            {
                case CLIENTPGC:
                    return _motoristsFromPGC;
                case CLIENTMCO:
                    return _motoristsFromMCO;
                case CLIENTNB:
                    return _motoristsFromNB;
                case CLIENTJAVE:
                    return _motoristsFromJAVE;
                case MOTORISTSTESTFROMPGC:
                    return _motoristsTestFromPGC;
                case MOTORISTSTESTFROMMCO:
                    return _motoristsTestFromMCO;
                case MOTORISTSTESTFROMNB:
                    return _motoristsTestFromNB;
                case MOTORISTSTESTFROMJAVE:
                    return _motoristsTestFromJAVE;
                default:
                    break;
            }
            return null;
        }

        private List<UserAccount> GetActiveAdminsByCategory(int clientId)
        {
            switch (clientId)
            {
                case ADMINPAYLOCK:
                    return _adminsFromPaylock;
                case ADMINDIGITALPERMITSAGENT:
                    return _adminsFromDigitalPermitsAgent;
                case ADMINPGC:
                    return _adminsFromPGC;
                case ADMINMCO:
                    return _adminsFromMCO;
                case ADMINNB:
                    return _adminsFromNB;
                case ADMINOTHER:
                    return _adminsFromOther;
                case ADMINTESTENGINEERS:
                    return _adminTestEngineers;
                case ADMINTESTACCOUNTMANAGERS:
                    return _adminTestAccountManagers;
                case ADMINTESTDIGITALPERMITSAGENTS:
                    return _adminTestDigitalPermitsAgents;
                default:
                    break;
            }
            return null;
        }

        private int NotifyUsersAboutSecurityPolicyUpdate(List<UserAccount> interactiveUsers)
        {
            var emailsSent = interactiveUsers.Count();

            if (_settings.PrintNotificationList)
            {
                PrintEmailsNotifiedList(interactiveUsers);
            }

            if (_settings.DoNotEmail == false)
            {
                emailsSent = EmailUsers(interactiveUsers);
            }

            return emailsSent;
        }

        private int NotifyUsersAboutSecurityPolicyUpdate(List<Motorist> interactiveUsers)
        {
            var emailsSent = interactiveUsers.Count();

            if (_settings.PrintNotificationList)
            {
                PrintEmailsNotifiedList(interactiveUsers);
            }

            if (_settings.DoNotEmail == false)
            {
                emailsSent = EmailUsers(interactiveUsers);
            }

            return emailsSent;
        }

        public void PrintEmailsNotifiedList(IEnumerable<UserAccount> userAccounts)
        {
            var sb = new StringBuilder();

            Console.WriteLine("Security Policy Emails sent for Client {0}", _settings.ClientId);
            foreach (var userAccount in userAccounts)
            {
                Console.WriteLine("{0}, {1} {2}",
                    userAccount.Email,
                    userAccount.FirstName,
                    userAccount.LastName);
               sb.AppendLine($" {userAccount.Email} {userAccount.FirstName} {userAccount.LastName}");
            }

            File.WriteAllText("C:\\temp\\SecurityPolicyNotificationsForParam-" + _settings.ClientId.ToString() + "-" + DateTime.Now.ToString("dd_MM_yyyy hh:mm:ss").Replace(':','-') + ".csv", sb.ToString());
        }

        public void PrintEmailsNotifiedList(IEnumerable<Motorist> motorists)
        {
            var sb = new StringBuilder();

            Console.WriteLine("Security Policy Emails sent for Client {0}", _settings.ClientId);
            foreach (var motorist in motorists)
            {
                Console.WriteLine("{0}, {1} {2}",
                    motorist.Email,
                    motorist.FirstName,
                    motorist.LastName);
                sb.AppendLine($" {motorist.Email} {motorist.FirstName} {motorist.LastName}");
            }

            File.WriteAllText("C:\\temp\\SecurityPolicyNotificationsForParam-" + _settings.ClientId.ToString() + "-" + DateTime.Now.ToString("dd_MM_yyyy hh:mm:ss").Replace(':', '-') + ".csv", sb.ToString());
        }

        private Client GetMotoristClient()
        {
            switch (_settings.ClientId)
            {
                case MOTORISTSTESTFROMPGC:
                    return _adminRepository.GetClient(6);
                case MOTORISTSTESTFROMMCO:
                    return _adminRepository.GetClient(9);
                case MOTORISTSTESTFROMNB:
                    return _adminRepository.GetClient(11);
                case MOTORISTSTESTFROMJAVE:
                    return _adminRepository.GetClient(12);
                default:
                    return _adminRepository.GetClient(_settings.ClientId);
            }     
        }

        public int EmailUsers(IEnumerable<Motorist> motorists)
        {
            var emailsDelivered = 0;

            foreach (var motorist in motorists)
            {

                var messageSent = _notifier.OurNewSecurityPolicy(_motoristClient, motorist.Email, motorist.FirstName, motorist.LastName, motorist.Id);

                if (messageSent)
                {
                    emailsDelivered += 1;
                }
            }

            return emailsDelivered;
        }

        public int EmailUsers(IEnumerable<UserAccount> userAccounts)
        {
            var emailsDelivered = 0;

            foreach (var userAccount in userAccounts)
            {
                var messageSent = _notifier.OurNewSecurityPolicy(null, userAccount.Email, userAccount.FirstName, userAccount.LastName, userAccount.Id);

                if (messageSent)
                {
                    emailsDelivered += 1;
                }
            }

            return emailsDelivered;
        }

        public int SendTestEmail()
        {
            var emailSent = _notifier.SendTestEmail();
            return emailSent ? 1 : 0;
        }
    }
}
