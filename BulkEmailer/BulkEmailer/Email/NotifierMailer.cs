﻿using ActionMailer.Net.Standalone;
using DigitalPermits.BulkEmailer.Email.Models;

namespace DigitalPermits.BulkEmailer.Email
{
	public class NotifierMailer : RazorMailerBase
	{
		public override string ViewPath
		{
			// Email templates have the form "name.txt.cshtml" or "name.html.cshtml". 
			// The txt and html part of the name specifies whether the email is rendered as
			// text or html as ActionMailer can produce multi-part emails

			// this is the path in the project for all of the email views
			get { return @"Email\Views"; }
		}

		public RazorEmailResult OurNewSecurityPolicy(PolicyDetails model)
		{
			To.Add(model.Email);
			Subject = "Security Policy Update";
			return Email("OurNewSecurityPolicy", model);
		}

		public RazorEmailResult YourPermitIsExpiring(PolicyDetails model) {
			To.Add(model.Email);
			Subject = "Permit Expiration Notification";
			return Email("YourPermitIsExpiring", model);
		}

		public RazorEmailResult SendTestEmail(PolicyDetails model)
		{
			To.Add(model.Email);
			Subject = "Test Permit Expiration Email";
			return Email("TestEmail", model);
		}
	}
}
