﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Linq;
using ActionMailer.Net.Standalone;
using DigitalPermits.EntityModel.Entities;
using DigitalPermits.EntityModel.Histories;
using DigitalPermits.EntityModel.Repositories;
using DigitalPermits.Framework.Tracing;
using DigitalPermits.BulkEmailer.Email.Models;

namespace DigitalPermits.BulkEmailer.Email {
	public class Notifier : INotifier
	{
	    private ITraceWriter _traceWriter;
		private readonly IHistoryRepository _historyRepository;

		public Notifier(IHistoryRepository historyRepository) {
			_historyRepository = historyRepository;
            _traceWriter = new TraceWriter("Notifier");
		}

		public Notifier() : this(new HistoryRepository()) {
		}

		public bool OurNewSecurityPolicy(Client motoristClient, string emailAddress, string firstName, string lastName, Guid userAccountId) {
			var mailer = new NotifierMailer();
			var messageDelivered = false;
            var firstnameAnnotated = (string.IsNullOrWhiteSpace(firstName)) ? "NO FIRSTNAME" : firstName;
            var lastnameAnnotated = (string.IsNullOrWhiteSpace(lastName)) ? "NO LASTNAME" : lastName;

            try {
				var details = new PolicyDetails
				{
					Email = emailAddress,
                    FirstName = firstName,
                    LastName = lastName,
					ClientLink = GetSiteURL(motoristClient),
                    ClientInformation = motoristClient,  //receiver must check for null should only happen for internal accounts
                    Today = DateTime.Today,
                    IsAdmin = (motoristClient == null) ? true : false
                };


				var emailToSend = mailer.OurNewSecurityPolicy(details);
				if (!emailAddress.ToLower().EndsWith("guest@digitalpermits.com") && !emailAddress.ToLower().EndsWith("@duplicatedonotuse.com")) {
                    emailToSend.Deliver();
					messageDelivered = true;
 
                    _traceWriter.WriteTrace(TraceEventType.Information,
                        $"Sent New Security Policy email to {firstnameAnnotated} {lastnameAnnotated} <{emailAddress}>");

                    recordMessageHistory(emailToSend, userAccountId, userAccountId);
                } else
                {
                    _traceWriter.WriteTrace(TraceEventType.Information,
                        $"Refused to Send New Security Policy email to {firstnameAnnotated} {lastnameAnnotated} <{emailAddress}>");
                }


				return messageDelivered;
			}
			catch (Exception ex)
			{
			    _traceWriter.WriteTrace(TraceEventType.Error, "Failed to deliver New Security Policy email",
			        new Dictionary<string, object>() {{"Email", emailAddress} });
                _traceWriter.WriteException(ex);
			}

			return messageDelivered;
		}

        private string GetSiteURL(Client motoristClient)
        {
            if (motoristClient == null)
            {
                return "https://permitview.digitalpermits.com/Account/LogOn";
            } else
            {
                return "https://" + motoristClient.SiteTheme.CssTheme + ".digitalpermits.com" + "/Account/LogOn";
            }
        }

		public bool SendTestEmail() {
			var mailer = new NotifierMailer();

			try {
				var details = new PolicyDetails()
				{
					Email = "test.user@digitalpermits.com",
					ClientLink = "https://test.digitalpermits.com",
					ClientInformation = new Client()
				};

				var email = mailer.SendTestEmail(details);
				email.Deliver();

                _traceWriter.WriteTrace(TraceEventType.Information, "Test Email Sent Successfully");

				return true;
			}
			catch (Exception ex)
			{
			    _traceWriter.WriteTrace(TraceEventType.Error, "Test email failed to deliver");
                _traceWriter.WriteException(ex);
			}

			return false;
		}
		
		private void recordMessageHistory(RazorEmailResult message, Guid userAccountId, Guid? createdById)
		{
            try
            {
                // Get the body of the email
                var messageBody = GetMessageBody(message);
                var createdBy = (createdById.HasValue) ? createdById.Value : Guid.Empty;


                var emailHistory = new EmailHistory
                {
                    To = message.Mail.To.ToString(),
                    From = message.Mail.From.ToString(),
                    Subject = message.Mail.Subject,
                    SentDate = DateTime.Now,
                    Body = messageBody
                };

                var recordToken = _historyRepository.RecordEmail(createdBy, userAccountId, emailHistory);
            } catch (Exception ex)
            {
                _traceWriter.WriteTrace(TraceEventType.Error, "Failed to log history of New Security Policy email",
                    new Dictionary<string, object>() { { "Email", message.Mail.To.ToString() } });
                _traceWriter.WriteException(ex);
            }

		}


		private string GetMessageBody(RazorEmailResult message)
		{
			// Get the  the AlternateViews property. When rendering the message, an alternate view is created
			// for each mail format (i.e. one for HTML and one for plain text). Each AlternateView object has 
			// a "ContentStream" property that you can use to access the body.
			var bodyStream = new StreamReader(message.Mail.AlternateViews[0].ContentStream);

			// seek to the beginning of the stream
			bodyStream.BaseStream.Seek(0, 0);

			// re-read the stream as a text string
			var messageBody = bodyStream.ReadToEnd();

			// closes the stream object so that the message object can be release later
			bodyStream.Close();

			return messageBody;
		}
	}
}
