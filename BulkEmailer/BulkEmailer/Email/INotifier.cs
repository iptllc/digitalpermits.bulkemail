﻿using System;
using DigitalPermits.EntityModel.Entities;

namespace DigitalPermits.BulkEmailer.Email
{
	public interface INotifier
	{
		bool OurNewSecurityPolicy(Client motoristClient, string emailAddress, string firstName, string lastName, Guid userAccountId);
		bool SendTestEmail();
	}
}
