﻿using System;
using DigitalPermits.EntityModel.Entities;

namespace DigitalPermits.BulkEmailer.Email.Models {
	public class PolicyDetails {
		public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClientLink { get; set; }
		public Client ClientInformation { get; set; }
        public DateTime Today { get; set; }
        public bool IsAdmin { get; set; }
    }
}
