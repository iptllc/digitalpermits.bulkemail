﻿using System;

namespace DigitalPermits.BulkEmailer 
{
	public class Settings {
		
		public Settings()
		{
			ClientId = 0;
			ListClientIds = false;
			ShowHelp = false;
			PrintNotificationList = true;
			DoNotEmail = false;
			Today = DateTime.Today;
		}

		public int ClientId { get; set; }
		public bool ListClientIds { get; set; }
		public bool ShowHelp { get; set; }
		public bool PrintNotificationList { get; set; }
		public bool DoNotEmail { get; set; }
		public DateTime Today { get; set; }
		public bool TestEmail { get; set; }
	}
}
